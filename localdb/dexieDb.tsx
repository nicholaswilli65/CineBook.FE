import Dexie, { Table } from 'dexie';

export interface Movie {
    movieId?: number,
    title: string,
    synopsis: string,
    startDate: Date | null,
    endDate: Date | null
}

export class DexieDb extends Dexie {
    movies!: Table<Movie>;

    constructor() {
        super('myDatabase');
        this.version(1).stores({
            movies: '++movieId'
        });
    }
}

export const db = new DexieDb();