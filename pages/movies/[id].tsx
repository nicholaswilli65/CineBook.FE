import { GetServerSideProps } from 'next';
import React from 'react';
import { WithDefaultLayout } from '../../components/DefautLayout';
import { Title } from '../../components/Title';
import { Page } from '../../types/Page';
import useSWR from 'swr';

interface PageProps {
    id: string
}

interface Movies {
    movieId: number,
    title: string,
    synopsis: string,
    startDate: string,
    endDate: string
}

const fetcher = async (url: string) => {
    return await fetch(url).then((res) => res.json());
}

const MoviePage: Page<PageProps> = ({ id }) => {
    const { data, error } = useSWR(`/api/demo/api/Movie/get-movie?MovieId=${id}`, fetcher);

    if (error) {
        return <div>Failed to load</div>;
    }
    if (!data) {
        return <div>Loading...</div>;
    }

    const movies = data as Movies[];
    if (movies.length !== 1) {
        return <></>;
    }
    const movie = movies[0];

    return (
        <div>
            <Title name="Movie"></Title>
            <h1>This is Movie Page</h1>
            <h2>Movie ID: {movie?.title}</h2>
            <h2>Synopsis: {movie?.synopsis}</h2>
            <h2>Start Date: {movie?.startDate}</h2>
            <h2>End Date: {movie?.endDate}</h2>
        </div>
    );
}

export const getServerSideProps: GetServerSideProps<PageProps> = async (context) => {
    if (!context) {
        return {
            props: {
                id: ''
            }
        };
    }
    if (!context.params) {
        return {
            props: {
                id: ''
            }
        };
    }

    const id = context.params['id'];
    if (typeof id !== 'string') {
        return {
            props: {
                id: ''
            }
        };
    }

    return {
        props: {
            id: id
        }
    };
}

MoviePage.layout = WithDefaultLayout;
export default MoviePage;
