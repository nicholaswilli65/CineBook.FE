import { GetServerSideProps } from 'next';
import React from 'react';
import { WithDefaultLayout } from '../../components/DefautLayout';
import { Title } from '../../components/Title';
import { Page } from '../../types/Page';

interface PageProps {
    id: string
}

const EditPage: Page<PageProps> = ({ id }) => {
    return (
        <div>
            <Title name="Home"></Title>
            <h1>This is Edit Page</h1>
            <h2>ID: {id}</h2>
        </div>
    );
}

export const getServerSideProps: GetServerSideProps<PageProps> = async (context) => {
    if (!context) {
        return {
            props: {
                id: ''
            }
        };
    }
    if (!context.params) {
        return {
            props: {
                id: ''
            }
        };
    }

    const id = context.params['id'];
    if (typeof id !== 'string') {
        return {
            props: {
                id: ''
            }
        };
    }

    return {
        props: {
            id: id
        }
    };
}

EditPage.layout = WithDefaultLayout;
export default EditPage;
